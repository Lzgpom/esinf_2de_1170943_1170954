package esinf.tp1.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import esinf.tp1.model.Station;
import esinf.tp1.model.Ticket;
import esinf.tp1.model.Trip;
import esinf.tp1.model.impl.StationImpl;
import esinf.tp1.model.impl.TicketImpl;
import esinf.tp1.model.impl.TripImpl;

public class FileImporter 
{
	private static final String SPLITTER_REGEX = ",";
	
	/**
	 * Imports a list of {@link Station stations} from a file.
	 * 
	 * @param fileLocation The file location.
	 * @return A list of {@link Station stations}.
	 */
	public static List<Station> importStations(String fileLocation)
	{
		List<Station> stations = new ArrayList<>();
		
		try 
		{
			Scanner input = new Scanner(new File(fileLocation));
			
			while(input.hasNext())
			{
				String[] params = input.nextLine().split(SPLITTER_REGEX);
				
				//Creates a station and adds to the list.
				stations.add(new StationImpl(Integer.parseInt(params[0]), params[1], params[2]));
			}
			
			input.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		
		return stations;
	}
	
	
	/**
	 * Imports a list of {@link Trip trips} from a file.
	 * 
	 * @param fileLocation The file location.
	 * @return A list of {@link Trip trips}.
	 */
	public static List<Trip> importTrips(String fileLocation)
	{
		List<Trip> trips = new ArrayList<>();
		
		try 
		{
			Scanner input = new Scanner(new File(fileLocation));
			
			while(input.hasNext())
			{
				String[] params = input.nextLine().split(SPLITTER_REGEX);
				
				Ticket ticket = new TicketImpl(Integer.parseInt(params[0]), params[1]);
				
				//Creates a trip and adds to the list.
				trips.add(new TripImpl(ticket, Integer.parseInt(params[2]), Integer.parseInt(params[3])));
			}
			
			input.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		
		return trips;
	}
}
