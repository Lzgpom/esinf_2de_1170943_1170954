package esinf.tp1.model;

import java.util.List;

/**
 * This represents a Station.
 * 
 * @author Luis Marques
 */
public interface Station extends Comparable<Station>
{
	/**
	 * @return The description of the {@link Station station}.
	 */
	int getId();
	
	/**
	 * @return The description of the {@link Station station}.
	 */
	String getDescription();
	
	/**
	 * @return The zone of the {@link Station station}.
	 */
	String getZone();
	
	/**
	 * @return The list of {@link Ticket tickets} validated in the entrance.
	 */
	List<Ticket> getInTickets();
	
	/**
	 * Adds a {@link Ticket ticket} to the list of validated in the entrance.
	 * 
	 * @param ticket The {@link Ticket ticket} validated in the entrance.
	 * @return Whether it was added.
	 */
	boolean addInTicket(Ticket ticket);
	
	/**
	 * @return The list of {@link Ticket tickets} validated in the exit.
	 */
	List<Ticket> getOutTickets();
	
	/**
	 * Adds a {@link Ticket ticket} to the list of validated in the exit.
	 * 
	 * @param ticket The {@link Ticket ticket} validated in the exit.
	 * @return Whether it was added.
	 */
	boolean addOutTicket(Ticket ticket);
	
	/**
	 * Adds one to the count of user that have passed on this station.
	 */
	void addToCountOfUserPassed();
	
	/**
	 * @return The number of users that have passed on this station.
	 */
	int getCountOfUserPassed();
}
