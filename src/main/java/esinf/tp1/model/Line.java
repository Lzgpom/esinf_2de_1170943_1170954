package esinf.tp1.model;

import java.util.List;

public interface Line 
{
	/**
	 * Adds a {@link Station station} to the line.
	 * The stations should be added in order.
	 * 
	 * @param station The {@link Station station} to add.
	 */
	void addStation(Station station);
	
	/**
	 * Adds a {@link Trip trip}.
	 * When adding it iterates through all the stations where 
	 * the ticket passes by.
	 * 
	 * @param trip The {@link Trip trip} to add.
	 */
	void addTrip(Trip trip);
	
	/**
	 * Checks if the {@link Line line} already has the station.
	 * 
	 * @param station The {@link Station station} to check.
	 * @return True if it contains the station.
	 */
	boolean contains(Station station);
	
	/**
	 * Calculates a list of the longest sequences, lists, of {@link Station stations}
	 * for the specific type of the {@link Ticket ticket].
	 * 
	 * @param ticket The {@link Ticket ticket] with the type of zone.
	 * @return A list of the longest sequences, lists, of {@link Station stations}
	 */
	List<List<Station>> getLongestSequencesForTicket(Ticket ticket);
	
	/**
	 * Checks whether the {@link Ticket ticket} is in transgression given a 
	 * {@link Trip trip}.
	 * 
	 * @param trip The {@link Trip trip} with the necessary information.
	 * @return The whether ticket is in transgression.
	 */
	boolean isTicketInTransgression(Trip trip);
}
