package esinf.tp1.model.impl;

import esinf.tp1.model.Ticket;

public class TicketImpl implements Ticket
{
	private final int id;
	private final String type;
	private int numberZones;
	
	/**
	 * Creates an instance of {@link Ticket Ticket}.
	 * 
	 * @param id The id of the {@link Ticket ticket}.
	 * @param zone The zone of the {@link Ticket ticket}.
	 * @param numberZones The number of zones the ticket can {@link Ticket ticket}.
	 */
	public TicketImpl(int id, String type)
	{
		this.id = id;
		this.type = type;
		setNumberZones(type);
	}
	
	@Override
	public int getId() 
	{
		return id;
	}

	@Override
	public String getType() 
	{
		return type;
	}

	@Override
	public int getNumberZones()
	{
		return numberZones;
	}
	
	/**
	 * Sets the number of zones the ticket can reach.
	 * 
	 * @param zone The zone of the {@link Ticket ticket}.
	 * @throws NumberFormatException If the type is not in the correct format. 
	 */
	private void setNumberZones(String type)
	{
		this.numberZones = Integer.parseInt(type.substring(1));
	}
	
}
