package esinf.tp1.model.impl;

import esinf.tp1.model.Ticket;
import esinf.tp1.model.Trip;

public class TripImpl implements Trip
{
	private final Ticket ticket;
	private final int startStationId;
	private final int endStationId;
	
	/**
	 * Creates an instance of {@link Trip Trip}.
	 * 
	 * @param ticket The {@link Ticket ticket} of the trip.
	 * @param startStationId The id of the station where the trip starts.
	 * @param endStationIdThe id of the station where the trip ends.
	 * @throws IllegalArgumentException If the start and end id of the stations are the same.
	 */
	public TripImpl(Ticket ticket, int startStationId, int endStationId)
	{
		this.ticket = ticket;
		
		if(startStationId == endStationId)
		{
			throw new IllegalArgumentException();
		}
		
		this.startStationId = startStationId;
		this.endStationId = endStationId;
	}
	
	@Override
	public Ticket getTicket() 
	{
		return ticket;
	}

	@Override
	public int getStartStationId() 
	{
		return startStationId;
	}

	@Override
	public int getEndStationId() 
	{
		return endStationId;
	}

}
