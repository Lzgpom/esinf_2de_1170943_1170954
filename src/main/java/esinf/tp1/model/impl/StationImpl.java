package esinf.tp1.model.impl;

import java.util.ArrayList;
import java.util.List;

import esinf.tp1.model.Station;
import esinf.tp1.model.Ticket;

public class StationImpl implements Station
{
	private final int id;
	private final String description;
	private final String zone;
	private final List<Ticket> in;
	private final List<Ticket> out;
	private int count;
	
	/**
	 * Creates an instance of {@link Station Station}.
	 * 
	 * @param description The description of the {@link Station station}.
	 */
	public StationImpl(int id, String description, String zone)
	{
		this.id = id;
		this.description = description;
		this.zone = zone;
		this.in = new ArrayList<>();
		this.out = new ArrayList<>();
		this.count = 0;
	}
	
	@Override
	public int getId() 
	{
		return id;
	}
	
	@Override
	public String getDescription() 
	{
		return description;
	}
	
	@Override
	public String getZone() 
	{
		return zone;
	}

	@Override
	public List<Ticket> getInTickets()
	{
		return in;
	}
	
	@Override
	public boolean addInTicket(Ticket ticket) 
	{
		return in.add(ticket);
	}

	@Override
	public List<Ticket> getOutTickets() 
	{
		return out;
	}
	
	@Override
	public boolean addOutTicket(Ticket ticket)
	{
		return out.add(ticket);
	}
	
	@Override
	public void addToCountOfUserPassed() 
	{
		count++;
	}

	@Override
	public int getCountOfUserPassed() 
	{
		return count;
	}	
	
	@Override
	public int hashCode()
	{
		int hash = id;
		hash = hash * 31 + description.hashCode();
		hash = hash * 31 + zone.hashCode();
		hash = hash * 31 + in.hashCode();
		return hash * 31 + out.hashCode();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
		{
			return false;
		}
		
		if(!(obj instanceof StationImpl))
		{
			return false;
		}
		
		StationImpl other = (StationImpl) obj;
		
		return this.id == other.id && this.description.equals(other.description) && this.zone.equals(other.zone)
				&& this.in.equals(other.in) && this.out.equals(other.out);
	}
	
	/**
	 * The compares using the id of each.
	 * @param station The other {@link Station station} to compare to.
	 */
	@Override
	public int compareTo(Station other)
	{
		return this.id - other.getId();
	}
}
