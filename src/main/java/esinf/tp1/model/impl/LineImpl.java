package esinf.tp1.model.impl;

import java.util.ArrayList;
import java.util.List;

import esinf.tp1.model.Line;
import esinf.tp1.model.Station;
import esinf.tp1.model.Ticket;
import esinf.tp1.model.Trip;

/**
 * This {@link Line Line} implementation ends up being a
 * Doubly Linked List.
 * 
 * @author Luis Marques
 */
public class LineImpl implements Line
{
	private Node head;
	private Node tail;
	private int size;
	
	/**
	 * Creates an instance of {@link Line Line}.
	 */
	public LineImpl()
	{
		size = 0;
	}
	
	/**
	 * This node is used to keep track of each element information.
	 */
	private class Node 
	{
        Station station;
        Node next;
        Node prev;
 
        public Node(Station station, Node next, Node prev)
        {
            this.station = station;
            this.next = next;
            this.prev = prev;
        }
	}
	
	@Override
	public void addStation(Station station) 
	{
		Node tmp = new Node(station, null, tail);
        if(tail != null)
        {
        	tail.next = tmp;
        }
        
        tail = tmp;
       
        if(head == null) 
        {
        	head = tmp;
        }
        
        size++;		
	}
	
	@Override
	public boolean contains(Station station) 
	{
		Node tmp = head;
        while(tmp != null)
        {
        	if(tmp.station.getId() == station.getId())
        	{
        		return true;
        	}
        	
            tmp = tmp.next;
        }
        
        return false;
	}

	@Override
	public void addTrip(Trip trip) 
	{
		//Is used to check in what direction the trip is.
		boolean forward = false;
		
		//The current node with the station.
		Node tmp;
		
		if(trip.getStartStationId() < trip.getEndStationId())
		{
			forward = true;
			tmp = head;
		}
		
		else
		{
			tmp = tail;
		}
		
		boolean foundStart = false;
		
        while(tmp != null)
        {
            if(!foundStart)
            {
            	if(tmp.station.getId() == trip.getStartStationId())
            	{
                	tmp.station.addToCountOfUserPassed();            	
            		tmp.station.addInTicket(trip.getTicket());
            		foundStart = true;
            	}
            }
            
            else
            {
            	tmp.station.addToCountOfUserPassed();            
            	
            	if(tmp.station.getId() == trip.getEndStationId())
            	{
            		tmp.station.addOutTicket(trip.getTicket());
            		break;
            	}
            }
            
            if(forward)
            {
                tmp = tmp.next;
            }
            
            else
            {
            	tmp = tmp.prev;
            }
        }
	}

	@Override
	public List<List<Station>> getLongestSequencesForTicket(Ticket ticket) 
	{
		int longestSequence = 0;
		List<List<Station>> sequences = new ArrayList<>();
		
		Node tmp = head;
		int i = 0;
		
		while(tmp != null)
        {
			//If this doesn't happen it means that there is no other sequence relevant.
			if(size - i >= longestSequence)
			{
				List<Station> sequence = getLongestSequenceWithStart(tmp, ticket.getNumberZones());
				if(sequence.size() > longestSequence)
				{
					sequences.clear();
					sequences.add(sequence);
					longestSequence = sequence.size();
				}
				
				else if(sequence.size() == longestSequence)
				{
					sequences.add(sequence);
				}
				
				i++;
				tmp = tmp.next;
			}
			
			else
			{
				return sequences;
			}
        }
		
		return sequences;
	}
	
	/**
	 * Creates the longest sequence of {@link Station stations} given a start and 
	 * the number of zones that it can go.
	 * 
	 * @param node The current node that it's checking.
	 * @param numberZones The number of zones.
	 * @return The longest sequence of {@link Station stations}
	 */
	private List<Station> getLongestSequenceWithStart(Node node, int numberZones)
	{
		List<Station> sequence = new ArrayList<>();
		
		Node tmp = node;
		
		while(tmp != null)
        {
			sequence.add(tmp.station);
			
			if(tmp.next != null)
			{				
				if(!tmp.station.getZone().equals(tmp.next.station.getZone()))
				{
					numberZones--;
				}
				
				if(numberZones < 1)
				{
					return sequence;
				}
				
			}
			
			tmp = tmp.next;
        }
		
		return sequence;
	}

	@Override
	public boolean isTicketInTransgression(Trip trip)
	{
		//Is used to check in what direction the trip is.
		boolean forward = false;
		
		//The current node with the station.
		Node tmp;
				
		if(trip.getStartStationId() < trip.getEndStationId())
		{
			forward = true;
			tmp = head;
		}
				
		else
		{
			tmp = tail;
		}
				
		boolean foundStart = false;
		
		//The number of zones the ticket has traveled.
		int numberZones = 1;
				
		while(tmp != null)
		{
			if(!foundStart)
			{
				if(tmp.station.getId() == trip.getStartStationId())
				{
					foundStart = true;
				}
			}
			
			else
			{
				if(forward)
				{
					if(!tmp.prev.station.getZone().equals(tmp.station.getZone()))
					{
						numberZones++;
					}
				}
				else
				{
					if(!tmp.next.station.getZone().equals(tmp.station.getZone()))
					{
						numberZones++;
					}
				}
				
		            	
				if(tmp.station.getId() == trip.getEndStationId())
				{
					break;
				}
			}
			
			if(forward)
			{
				tmp = tmp.next;
			}
			
			else
			{
				tmp = tmp.prev;
			}
		}
		
		return numberZones > trip.getTicket().getNumberZones();
	}
}
