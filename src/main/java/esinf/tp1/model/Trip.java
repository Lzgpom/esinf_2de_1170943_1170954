package esinf.tp1.model;

/**
 * This represents a Trip.
 * 
 * @author Luis Marques
 */
public interface Trip
{
	/**
	 * @return The {@link Ticket ticket} of the trip.
	 */
	Ticket getTicket();
	
	/**
	 * @return The id of the {@link Station station} from where the trip started.
	 */
	int getStartStationId();
	
	/**
	 * @return The id of the {@link Station station} from where the trip ended.
	 */
	int getEndStationId();
}
