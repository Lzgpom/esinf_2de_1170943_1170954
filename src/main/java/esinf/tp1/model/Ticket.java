package esinf.tp1.model;

/**
 * This is an Ticket.
 * 
 * @author Luis Marques
 */
public interface Ticket 
{
	/**
	 * @return The id of the {@link Ticket ticket}.
	 */
	int getId();
	
	/**
	 * @return The type of the {@link Ticket ticket}.
	 */
	String getType();
	
	/**
	 * @return The number of zones the {@link Ticket ticket} can reach.
	 */
	int getNumberZones();
}
