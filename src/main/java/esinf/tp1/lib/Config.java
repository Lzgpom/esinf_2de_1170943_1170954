package esinf.tp1.lib;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config 
{
	private static final String PROPERTIES_FILE = "config.properties";
	private static final String TRIPS_PROPERTY_NAME = "trips";
	private static final String STATION_PROPERTY_NAME = "stations";
	
	public static String TRIPS_FILE;
	public static String STATIONS_FILE;
	
	public static void loadConfigs()
	{
		Properties prop = new Properties();
		InputStream input = null;

		try
		{
			input = new FileInputStream(PROPERTIES_FILE);

			prop.load(input);

			TRIPS_FILE = prop.getProperty(TRIPS_PROPERTY_NAME);
			STATIONS_FILE = prop.getProperty(STATION_PROPERTY_NAME);
		} 
		
		catch (IOException ex)
		{
			ex.printStackTrace();
		} 
		finally 
		{
			if (input != null)
			{
				try 
				{
					input.close();
				}
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
}
