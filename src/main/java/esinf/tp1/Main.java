package esinf.tp1;

import java.util.Collections;
import java.util.List;

import esinf.tp1.io.FileImporter;
import esinf.tp1.lib.Config;
import esinf.tp1.model.Line;
import esinf.tp1.model.Station;
import esinf.tp1.model.Trip;
import esinf.tp1.model.impl.LineImpl;
import esinf.tp1.model.impl.TicketImpl;

public class Main 
{
	public static void main(String[] args)
	{
		Config.loadConfigs();
		List<Station> stations = FileImporter.importStations(Config.STATIONS_FILE);
		List<Trip> trips = FileImporter.importTrips(Config.TRIPS_FILE);
		Line line = new LineImpl();
	
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			line.addStation(station);
		}
		
		for(Trip trip : trips)
		{
			line.addTrip(trip);
		}
		
		List<List<Station>> sequences = line.getLongestSequencesForTicket(new TicketImpl(1170954, "z3"));
		
		for(List<Station> sequence : sequences)
		{
			printStations(sequence);
		}
	}
	
	private static void printStations(List<Station> sequence)
	{
		System.out.println("Sequence:");
		
		for(Station station : sequence)
		{
			System.out.printf("%-3d %-20s %s%n", station.getId(), station.getDescription(), station.getZone());
		}
	}
}
