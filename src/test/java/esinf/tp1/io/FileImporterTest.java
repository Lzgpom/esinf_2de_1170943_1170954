package esinf.tp1.io;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import esinf.tp1.model.Station;
import esinf.tp1.model.Trip;
import esinf.tp1.model.impl.StationImpl;

class FileImporterTest 
{
	
	@Test
	void ensureImportStationsWorks() 
	{
		String testFile = "target/test-classes/stations_test.txt";
		List<Station> expected = new ArrayList<>();
		expected.add(new StationImpl(2, "IPO", "C1"));
		expected.add(new StationImpl(1, "Hospital de S.Joao", "C1"));
		expected.add(new StationImpl(10, "Casa da Musica", "C4"));
		
		List<Station> result = FileImporter.importStations(testFile);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureImportStationsReturnsEmptyWhenFileIsWrong() 
	{
		List<Station> expected = new ArrayList<>();
		List<Station> result = FileImporter.importStations("Somewhere");
		assertEquals(expected, result);
	}
	
	@Test
	void ensureImportTripsWorks() 
	{
		String testFile = "target/test-classes/trips_test.txt";
		
		List<Trip> result = FileImporter.importTrips(testFile);
		assertEquals(111222333, result.get(0).getTicket().getId());
		assertEquals("Z2", result.get(0).getTicket().getType());
		assertEquals(1, result.get(0).getStartStationId());
		assertEquals(10, result.get(0).getEndStationId());
		
		assertEquals(111333222, result.get(1).getTicket().getId());
		assertEquals("Z3", result.get(1).getTicket().getType());
		assertEquals(1, result.get(1).getStartStationId());
		assertEquals(2, result.get(1).getEndStationId());
	}
	
	@Test
	void ensureImportTripsReturnsEmptyWhenFileIsWrong() 
	{
		List<Trip> expected = new ArrayList<>();
		List<Trip> result = FileImporter.importTrips("Somewhere");
		assertEquals(expected, result);
	}
}
