package esinf.tp1.model.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import esinf.tp1.io.FileImporter;
import esinf.tp1.model.Line;
import esinf.tp1.model.Station;
import esinf.tp1.model.Ticket;
import esinf.tp1.model.Trip;

class LineImplTest 
{
	LineImpl line;
	
	@BeforeEach
	void setUp()
	{
		line = new LineImpl();
		line.addStation(new StationImpl(8, "Trindade", "C1"));
	}
	
	@Test
	void ensureAddStationAndCointaisWorks()
	{
		Station station = new StationImpl(2, "IPO", "C6");
		assertEquals(false, line.contains(station));
		
		line.addStation(station);
		assertEquals(true, line.contains(station));
	}
	
	@Test
	void ensureAddTripsWorksFoward()
	{
		Line other = new LineImpl();
		List<Station> stations = FileImporter.importStations("target/test-classes/stations_line_test.txt");
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			other.addStation(station);
		}
		
		Ticket ticket = new TicketImpl(1170954, "z2");
		Trip trip = new TripImpl(ticket, 1, 6);
		other.addTrip(trip);
		
		assertEquals(stations.get(0).getInTickets().get(0), ticket);
		assertEquals(stations.get(2).getOutTickets().get(0), ticket);
		assertEquals(stations.get(0).getCountOfUserPassed(), 1);
		assertEquals(stations.get(1).getCountOfUserPassed(), 1);
		assertEquals(stations.get(2).getCountOfUserPassed(), 1);
	}
	
	@Test
	void ensureAddTripsWorksBackwards()
	{
		Line other = new LineImpl();
		List<Station> stations = FileImporter.importStations("target/test-classes/stations_line_test.txt");
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			other.addStation(station);
		}
		
		Ticket ticket = new TicketImpl(1170954, "z2");
		Trip trip = new TripImpl(ticket, 6, 1);
		other.addTrip(trip);
		
		assertEquals(stations.get(0).getOutTickets().get(0), ticket);
		assertEquals(stations.get(2).getInTickets().get(0), ticket);
		assertEquals(stations.get(0).getCountOfUserPassed(), 1);
		assertEquals(stations.get(1).getCountOfUserPassed(), 1);
		assertEquals(stations.get(2).getCountOfUserPassed(), 1);

	}
	
	@Test
	void ensureGetLongestSequencesForTicketWorksGenerally()
	{
		Line other = new LineImpl();
		List<Station> stations = FileImporter.importStations("target/test-classes/stations_line_test.txt");
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			other.addStation(station);
		}
		
		List<List<Station>> expected = new ArrayList<>();
		
		List<Station> expected1 = new ArrayList<>();
		expected1.add(stations.get(1));
		expected1.add(stations.get(2));
		expected1.add(stations.get(3));
		expected1.add(stations.get(4));
		expected.add(expected1);
				
		List<Station> expected2 = new ArrayList<>();
		expected2.add(stations.get(3));
		expected2.add(stations.get(4));
		expected2.add(stations.get(5));
		expected2.add(stations.get(6));
		expected.add(expected2);
		
		List<List<Station>> result = other.getLongestSequencesForTicket(new TicketImpl(1170954, "z2"));
		assertEquals(expected, result);
	}
	
	@Test
	void ensureIsTicketInTransgressionReturnsTrue()
	{
		Line other = new LineImpl();
		List<Station> stations = FileImporter.importStations("target/test-classes/stations_line_test.txt");
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			other.addStation(station);
		}
		
		boolean expected = true;
		boolean result = other.isTicketInTransgression(new TripImpl(new TicketImpl(1170954, "z2"), 1, 12));
		
		assertEquals(expected, result);
	}
	
	@Test
	void ensureIsTicketInTransgressionReturnsFalse()
	{
		Line other = new LineImpl();
		List<Station> stations = FileImporter.importStations("target/test-classes/stations_line_test.txt");
		Collections.sort(stations);
		
		for(Station station : stations)
		{
			other.addStation(station);
		}
		
		boolean expected = false;
		boolean result = other.isTicketInTransgression(new TripImpl(new TicketImpl(1170954, "z3"), 12, 1));
		
		assertEquals(expected, result);
	}
}
