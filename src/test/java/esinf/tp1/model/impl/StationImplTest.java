package esinf.tp1.model.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import esinf.tp1.model.Station;
import esinf.tp1.model.Ticket;

class StationImplTest 
{
	StationImpl station;
	
	@BeforeEach
	void setUp()
	{
		station = new StationImpl(1, "Trindade", "C2");
	}
	
	@Test
	void ensureGetIdIsCorrect() 
	{
		int expected = 1;
		int result = station.getId();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureGetDescriptionIsCorrect() 
	{
		String expected = "Trindade";
		String result = station.getDescription();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureGetZoneIsCorrect() 
	{
		String expected = "C2";
		String result = station.getZone();
		assertEquals(expected, result);
	}

	@Test
	void ensureGetInTicketsIsCorrect() 
	{
		List<Ticket> expected = new ArrayList<>();
		List<Ticket> result = station.getInTickets();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureAddInTicketWorks()
	{
		Ticket ticket = new TicketImpl(1170954, "z3");
		station.addInTicket(ticket);
		
		List<Ticket> expected = new ArrayList<>();
		expected.add(ticket);
		List<Ticket> result = station.getInTickets();
		assertEquals(expected, result);
	}

	@Test
	void ensureGetOutTicketsIsCorrect()
	{
		List<Ticket> expected = new ArrayList<>();
		List<Ticket> result = station.getOutTickets();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureAddOutTicketWorks()
	{
		Ticket ticket = new TicketImpl(1170954, "z3");
		station.addOutTicket(ticket);
		
		List<Ticket> expected = new ArrayList<>();
		expected.add(ticket);
		List<Ticket> result = station.getOutTickets();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureHashCodeIsCorrect()
	{
		int expected = 1810067471;
		int result = station.hashCode();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsNullReturnsFalse()
	{
		boolean expected = false;
		boolean result = station.equals(null);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsNotStationReturnsFalse()
	{
		boolean expected = false;
		boolean result = station.equals(new Object());
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsDifferentIdReturnsFalse()
	{
		boolean expected = false;
		
		StationImpl other = new StationImpl(2, "Trindade", "C2");
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsDifferentDescriptionReturnsFalse()
	{
		boolean expected = false;
		
		StationImpl other = new StationImpl(1, "Pias", "C2");
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsDifferentZoneReturnsFalse()
	{
		boolean expected = false;
		
		StationImpl other = new StationImpl(1, "Trindade", "C3");
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsDifferentInTicketsReturnsFalse()
	{
		boolean expected = false;
		
		StationImpl other = new StationImpl(1, "Trindade", "C2");
		other.addInTicket(new TicketImpl(1170954, "z3"));
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualsDifferentOutTicketsReturnsFalse()
	{
		boolean expected = false;
		
		StationImpl other = new StationImpl(1, "Trindade", "C2");
		other.addOutTicket(new TicketImpl(1170954, "z3"));
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureEqualObjectReturnsTrue()
	{
		boolean expected = true;		
		StationImpl other = new StationImpl(1, "Trindade", "C2");
		boolean result = station.equals(other);
		assertEquals(expected, result);
	}
	
	@Test
	void ensureCompareToIsCorrect()
	{
		Station other = new StationImpl(2, "IPO", "C6");
		boolean expected = true;
		boolean result = station.compareTo(other) < 0;
		
		assertEquals(expected, result);
	}
	
	@Test
	void ensureGetAndAddCountIsCorrect()
	{
		station.addToCountOfUserPassed();
		int expected = 1;
		int result = station.getCountOfUserPassed();
		assertEquals(expected, result);
	}
}
