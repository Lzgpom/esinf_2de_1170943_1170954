package esinf.tp1.model.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import esinf.tp1.model.Ticket;

class TripImplTest
{
	TripImpl trip;
	Ticket ticket = new TicketImpl(1170954, "z3");
	

	@BeforeEach
	void setUp()
	{
		trip = new TripImpl(ticket, 1, 10);
	}
	
	@Test
	public void ensureInvalidTripThorwsAnException()
	{ 
		assertThrows(IllegalArgumentException.class, () -> {
	        new TripImpl(ticket, 2, 2);});
	}
	
	@Test
	void ensureGetTicketIsCorrect() 
	{
		Ticket expected = ticket;
		Ticket result = trip.getTicket();
		assertEquals(expected, result);
	}

	@Test
	void ensureGetStartStationIdIsCorrect() 
	{
		int expected = 1;
		int result = trip.getStartStationId();
		assertEquals(expected, result);
	}

	@Test
	void ensureGetEndStationIdIsCorrect() 
	{
		int expected = 10;
		int result = trip.getEndStationId();
		assertEquals(expected, result);
	}

}
