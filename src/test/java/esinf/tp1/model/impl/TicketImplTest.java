package esinf.tp1.model.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TicketImplTest 
{
	TicketImpl ticket;
	
	@BeforeEach
	void setUp()
	{
		ticket = new TicketImpl(1170954, "z3");
	}
	
	@Test
	public void ensureInvalidTypeThorwsAnException()
	{ 
		assertThrows(NumberFormatException.class, () -> {
	        new TicketImpl(1170954, "zz");});
	}
	
	@Test
	void ensureGetIdIsCorrect() 
	{
		int expected = 1170954;
		int result = ticket.getId();
		assertEquals(expected, result);
	}

	@Test
	void ensureGetTypeIsCorrect()
	{
		String expected = "z3";
		String result = ticket.getType();
		assertEquals(expected, result);
	}
	
	@Test
	void ensureGetNumberZonesIsCorrect()
	{
		int expected = 3;
		int result = ticket.getNumberZones();
		assertEquals(expected, result);
	}
}
