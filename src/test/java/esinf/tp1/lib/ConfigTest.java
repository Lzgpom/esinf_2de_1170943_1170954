package esinf.tp1.lib;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ConfigTest {

	@Test
	void ensureLoadConfigsWorks()
	{
		Config.loadConfigs();
		assertEquals("fx_estacoes.txt", Config.STATIONS_FILE);
		assertEquals("fx_viagens.txt", Config.TRIPS_FILE);
	}
}
